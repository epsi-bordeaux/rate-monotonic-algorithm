package main

import (
	"fmt"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

type State int

const (
	Wait State = 1 << iota
	Run
	Sleep
)

type Process struct {
	name        string
	period      int
	execution   int
	utilization float64
	state       State
}

func toInt(text string) int {
	i, _ := strconv.Atoi(text)
	return i
}

// tda Time Demand Analysis
func tda(processes []Process) bool {
	var done bool
	var schedulable bool
	sort.Slice(processes, func(i, j int) bool {
		return processes[i].period < processes[j].period
	})
	for !done {
		// Calculate time points
		// calculate w(t)
		// if w(t) >= t {
		// schedulable = false
		// return }

		// if all processes processed {
		done = true
		//}
	}
	return schedulable
}

//  Assign priorities to jobs in each task based on the period of that task
//
// + Shorter period → higher priority; rate (of job releases) is the inverse of the period, so jobs with higher rate have higher priority
// + Rationale: schedule jobs with most deadlines first, fit others around them
// + All jobs in a task have the same priority – fixed priority algorithm

func main() {
	fmt.Println("Rate Monotonic Analysis")

	data := "A runs every 50 msec for 20 msec\nB runs every 50 msec for 10 msec\nC runs every 30 msec for 10 msec"
	input := strings.Trim(data, "\n")

	re := regexp.MustCompile(`([A-Z]) runs every (\d+) msec for (\d+) msec`)
	out := re.FindAllStringSubmatch(input, -1)

	var processes = []Process{}

	for _, value := range out {
		process := Process{
			name:      value[1],
			period:    toInt(value[2]),
			execution: toInt(value[3]),
			state:     Wait,
		}
		process.utilization = float64(process.execution / process.period)
		fmt.Printf("%s runs every %d msec for %d msec\n", process.name, process.period, process.execution)
		processes = append(processes, process)
	}

	fmt.Println("Is the system schedulable?")

	numberOfProcesses := float64(len(processes))
	schedulability := numberOfProcesses * (math.Pow(2, 1/numberOfProcesses) - 1)

	var systemUtilization float64 = 0

	for _, process := range processes {
		systemUtilization += process.utilization
	}

	if systemUtilization < schedulability {
		fmt.Println("OK - The system is surely schedulable")
	} else {
		fmt.Println("WARNING - Scheduling might not work. Continue at your own risks...")
	}

	// Sort priority
	// Lowest period gets higher priority
	// First element in the list has highest priority
	sort.Slice(processes, func(i, j int) bool {
		return processes[i].period < processes[j].period
	})

	fmt.Println(processes)

	// The Clock
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	done := make(chan bool)

	// #################################################

	// Stop in 5 seconds
	// Tick per Second instead of Milliseconds to debug
	go func() {
		time.Sleep(5 * time.Second)
		done <- true
	}()

	for {
		select {
		case <-done:
			fmt.Println("END of Time")
			return
		case t := <-ticker.C:
			fmt.Println("Current time: ", t)
			fmt.Printf("Run task %s for %d sec\n", processes[0].name, processes[0].execution)
			// Do something
		}
	}
}
